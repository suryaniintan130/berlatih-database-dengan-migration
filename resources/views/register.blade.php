<!DOCTYPE html>
<html>
<head>
<title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1> 
    <h3>Sign Up Form</h3> 
<form action="welcome" method="POST">
    @csrf 
    <label>First name:</label><br><br>
    <input type="text" name="nama_awal"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="nama_akhir"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="GD" >Male <br>
    <input type="radio" name="GD" >Female <br>
    <input type="radio" name="GD" >Other <br><br>
    <label>Nationality:</label><br><br>
    <select name="NT">
        <option value="indonesian">Indonesian</option>
        <option value="malaysian">Malaysian</option>
        <option value="singaporean">Singaporean</option>
        <option value="australian">Australian</option>
    </select> <br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox" >Bahasa Indonesia <br>
    <input type="checkbox" >English <br>
    <input type="checkbox" >Other <br><br>
    <label>Bio:</label><br><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">

</form>   
</body>
</html>